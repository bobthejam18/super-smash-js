import { StreetFighterGame } from './StreetFighterGame.js';

windows.addEventListener('load', function() {
    new StreetFighterGame().start();
});
