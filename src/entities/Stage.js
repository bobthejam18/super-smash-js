export class Stage {
	constructor() {
		this.image = document.querySelector('img[alt="stage"]');
	
		this.frames = new Map([
			['stage-background', [72, 208, 768, 176]],
			['stage-background', [8, 16, 521, 180]],
			['stage-background', [8, 392, 896, 72]],
		]);
	}

	update() { }

	draw(context) {
		context.drawImage(this.image, 0, 0);
	}
}
